(function() {
	var images = ["http://s3.amazonaws.com/digitaltrends-uploads-prod/2013/12/beyonce.jpeg", "https://s-media-cache-ak0.pinimg.com/736x/23/72/d5/2372d57160bff7a186ecd31858ebdc31.jpg", "http://img.fifa.com/mm/photo/tournament/competition/02/23/87/72/2238772_big-lnd.jpg", "http://studylogic.net/wp-content/uploads/2013/01/burger.jpg", "http://npic.orst.edu/images/foodsafebnr.jpg", "http://www.iphonerepairscolchester.co.uk/wp-content/uploads/2014/11/computer-repairs-colchester.jpg", "http://boozwheez.co.uk//wp-content/uploads/2014/07/Smartphone.jpg", "http://i.telegraph.co.uk/multimedia/archive/03379/animals-smile_3379238k.jpg", "http://static.comicvine.com/uploads/original/11120/111208964/4674762-4997680223-beats.jpg", "http://cdn.wegotthiscovered.com/wp-content/uploads/Steam-Logo.jpg", "http://www.hovertravel.co.uk/imgCrop/CategoryDescriptions/0/IOW-Steam-Railway-2.jpg", "http://www.offroaders.com/Destinations/Lancaster/images/thomas-the-train-l.jpg", "http://i.telegraph.co.uk/multimedia/archive/03235/hagrid2_3235619b.jpg", "http://www4.cdn.sherdog.com/_images/pictures/20150712120138_5D3_0768.JPG",  "http://www.history.com/s3static/video-thumbnails/AETN-History_Prod/73/831/History_Speeches_6001_Titanic_Survivor_Eyewitness_still_624x352.jpg", "http://i.ytimg.com/vi/C_fEIVwjrew/maxresdefault.jpg", "http://www.clear-line.co.uk/wp-content/uploads/2014/11/Bullring-Shopping-Centre-Shop-Front-Install.jpg", "http://www.americancarwash.co.uk/wp-content/uploads/2013/07/Car1-5.jpg", "http://weknowyourdreams.com/images/sunset/sunset-02.jpg", "https://s-media-cache-ak0.pinimg.com/originals/f7/dc/be/f7dcbeb7280721bb6041ea21f3669c84.jpg", "http://jeffersondaynes.files.wordpress.com/2012/10/dominos.jpg", "http://www.ikea.com/PIAimages/29234_PE116289_S5.JPG", "http://irvinewatchrepair.com/wp-content/uploads/2012/11/grandfather-clock-repair-irvine.png", "http://www.blindsinn.com/wp-content/flagallery/fuax-blinds/custom_wood_blinds.jpg", "http://teacherluke.co.uk/wp-content/uploads/2009/11/All-doctors.png", "https://www.youngwriters.co.uk/images/resources/themed-packs/teacher-resources-romans.png", "http://isabelfarrell.com/images/roman_dolphin.jpg", "http://audioengineusa.com/P4B-front_3.jpg", "http://www.billboard.com/files/media/drake-cover-990.jpg", "http://i4.cdnds.net/12/48/618x450/screen-shot-2012-11-26-at-140633.jpg", "http://cdn.wegotthiscovered.com/wp-content/uploads/The-Fresh-Prince-of-Bel-Air-.jpg"];
	var ans = [];
	var options = [];
	var score = 0;
	var attempts = 0;
	var progress = 1;
	var gameover = false;
	$(function() {

		displayImage();

		$('#next-btn').click(function() {
			goToNext();
		});

		$('body').on('click', '.opt-item', function() {
			var $this = $(this);
			if ($this.hasClass('correct') || $this.hasClass('wrong')) {
				return;
			}

			var sel_opt = $(this).text();
			if (ans.indexOf(sel_opt) >= 0) {
				$this.addClass('correct');
				score++;
			} else {
				$this.addClass('wrong');
			}
			attempts++;
			console.log(attempts);
			$('.score').html(score);
			if (attempts >= 3) {
				$('.opt-item').each(function(i,e){
					console.log(i);
					var $this = $(e);
					if (ans.indexOf($this.text()) >= 0) {
						if(!$this.hasClass('correct')){
							$this.addClass('correction');
						}
					}
				});
				setTimeout(function() {
					goToNext();
					attempts = 0;
				}, 3500);
			}
		});
		
		$('.continue-btn').click(function(){
			$(".modal-container").fadeOut();
		});
	});

	function convertImgToBase64(url, callback, outputFormat) {
		var img = new Image();
		img.crossOrigin = 'Anonymous';
		img.onload = function() {
			var canvas = document.createElement('CANVAS');
			var ctx = canvas.getContext('2d');
			canvas.height = this.height;
			canvas.width = this.width;
			ctx.drawImage(this, 0, 0);
			var dataURL = canvas.toDataURL(outputFormat || 'image/png');
			callback(dataURL);
			canvas = null;
		};
		img.src = url;
	}

	function shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}

	function getImage() {
		console.log("old img length:"+images.length);
		//if (images.length <= 0) {
			if(progress>=5){
			//alert("gameover!");
			$('.modal-container').addClass('gameover').fadeIn();
			return;
		}
		var img_url;
		var rnd = Math.floor((Math.random() * images.length));
		img_url = images[rnd];
		images.splice(rnd, 1);
		console.log("new img length:"+images.length);
		return img_url;
	}

	function displayImage() {
		var img_url = getImage();
		$('#img-preview').attr('src', img_url);
		clearStuff();
		$.ajax({
			url : 'https://api.clarifai.com/v1/tag/?url=' + img_url,
			beforeSend : function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer vuag7Sln2CikWz07R7ImjfVZcQOZLe");
			},
			crossDomain : true,
			success : function(data) {
				console.log(data);
				
				var classes = data.results[0].result.tag.classes;
				for (var i = 0; i < 10; i++) {
					var tag = classes[i];
					if (i < 3) {
						ans.push(tag);
					}
					options.push(tag);
					console.log("##" + tag);
				}
				shuffle(options);

				for (var i = 0; i < options.length; i++) {

					$('.options').append('<div class="opt-item">' + options[i] + '</div>');
				}
			}
		});
	}

	function clearStuff() {
		$('.options').html("");
		ans = [];
		options = [];
	}

	function goToNext() {
		displayImage();
		if(!gameover){
			progress++;
		}
		$('#progress').html(progress);
	}

})();
